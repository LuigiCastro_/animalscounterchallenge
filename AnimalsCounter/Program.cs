﻿// INFORMAR: Nome + Tipo de 5 animais.
// EXIBIR quantidade de Cachorros, Gatos e Peixes.

/* Regras:
 * somente Cachorro, Gato e Peixe (diferente desses = Peixe).
 * Classe para representar animal.
 * Classe -> dois dados privados para representar as características do animal.
 * Classe -> métodos de acesso para permitir armazenar/ler dados dos dois dados privados.
*/


using System;
using System.Security.Cryptography.X509Certificates;
using Zoo;

namespace Zoo
{
    public class Animal
    {
        public string Name;
        public string Tyoe;  
    }
}

class Program
{   
    public static void Main(string[] args)
    {
        string[] animals_name = new string[5] {"A", "B", "C", "D", "E"};
        
        Console.WriteLine($"What the name of the your animals? ");
        animals_name[0] = Console.ReadLine();
        animals_name[1] = Console.ReadLine();
        animals_name[2] = Console.ReadLine();
        animals_name[3] = Console.ReadLine();
        animals_name[4] = Console.ReadLine();
        Console.Clear();

        Zoo.Animal animal1 = new Animal();
        Zoo.Animal animal2 = new Animal();
        Zoo.Animal animal3 = new Animal();
        Zoo.Animal animal4 = new Animal();
        Zoo.Animal animal5 = new Animal();

        string[] animals_type = new string[5] {animal1.Tyoe, animal2.Tyoe, animal3.Tyoe, animal4.Tyoe, animal5.Tyoe};

        Console.WriteLine();
        int x = 0;
        int y = 0;
        while (x >= 0 && x < animals_name.Length)
        {
            Console.Write($"Que animal é o(a) {animals_name[x]}? ");
            string resposta = Console.ReadLine();

            while (y >= 0 && y < animals_type.Length)
            {
                if (resposta.ToUpper() == "dog".ToUpper())
                {
                    animals_type[y] = resposta;
                }
                else if (resposta.ToUpper() == "cat".ToUpper())
                {
                    animals_type[y] = resposta;
                }
                else
                {
                    animals_type[y] = "fish";
                }
                y++;
                break;
            }
            x++;
        }
        Console.Clear();
        
        int z = 0;
        Console.WriteLine("RESUMO DOS ANIMAIS:");
        Console.WriteLine("-------------------------------");
        foreach (string animal in animals_name)
        {
            Console.Write($"O(A) {animal} é um ");
            while (z >= 0 && z < animals_type.Length)
            {
                Console.WriteLine(animals_type[z]);
                z++;
                break;
            }
        }
        
        int dogs = 0;
        int cats = 0;
        int fishs = 0;
        int i = 0;
        while (i >= 0 && i < animals_type.Length)
        {
            if (animals_type[i].ToUpper() == "dog".ToUpper())
            {
                dogs++;
            }
            else if (animals_type[i].ToUpper() == "cat".ToUpper())
            {
                cats++;
            }
            else
            {
                fishs++;
            }
            i++;
        }
        Console.WriteLine("-------------------------------");
        Console.WriteLine("Quantidade de cachorros: " + dogs);
        Console.WriteLine("Quantidade de gatos: " + cats);
        Console.WriteLine("Quantidade de peixes: " + fishs);
        Console.WriteLine("-------------------------------");
        Console.WriteLine($"Total de animais: {dogs + cats + fishs}");

        Console.ReadLine();
    }
}
